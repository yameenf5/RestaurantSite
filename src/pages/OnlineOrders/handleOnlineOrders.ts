import { supabaseClient } from "../../utils/supabaseClient";

export async function getOnlineOrder(){
  const { data, error } = await supabaseClient.from('Orders').select('*, CustomerDetails (*) ').eq('orderType', 'ONLINE')
  if (error) throw error
  if (data) return data
}
