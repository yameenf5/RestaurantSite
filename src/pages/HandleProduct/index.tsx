import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { deleteProduct, getCategories, getProductPicture, getProducts, reUploadImage, updateProduct } from "./handleProduct";
import { ColumnDef, Row } from "@tanstack/react-table";
import { typeMenuProductEdit, typeViewProductData } from "../../../types";
import { AiOutlineDelete, AiOutlineEdit } from "react-icons/ai";
import { MdOutlinePreview } from "react-icons/md";
import { SubmitHandler, useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import { useState } from "react";
import { ProductTable } from "../../components/Tables/ProductTable";
import Navbar from "../../components/Navbar";

export function HanldeProducts() {
  const { data, isLoading: isfetching } = useQuery(['getProducts'], getProducts)
  const column: ColumnDef<typeViewProductData>[] = [
    {
      header: 'Product Name',
      accessorKey: 'name'
    },
    {
      header: 'Category',
      accessorKey: 'category'
    },
    {
      header: 'Price In Rupees',
      accessorKey: 'price'
    },
    {
      header: 'Available',
      accessorFn: (row) => row.isAvailable ? 'Yes' : 'No'
    },
    {
      header: 'Description',
      accessorKey: 'description'
    },
    {
      header: 'Size',
      accessorKey: 'size'
    },
    {
      header: 'Action',
      cell: ({ row }) => {
        return (<>
          <ActionButton row={row} />
        </>
        )
      }
    }
  ]

  if (isfetching) return <div className="w-full text-center"><div className="h-1/4 w-1/4  loading loading-bars"></div></div>
  if (data)
    return (<>
      <Navbar />
      <ProductTable dataArray={data} column={column} />
    </>)
}



function ActionButton({ row }: { row: Row<typeViewProductData> }) {
  const queryClient = useQueryClient()
  const { mutate: deleteProductFn, isLoading: isDeleting } = useMutation(['deleteProduct'], deleteProduct, {
    onError: () => toast.error("INTERNAL SERVER ERROR"),
    onSuccess: () => {
      toast.success("Product Deleted");
      queryClient.invalidateQueries(['getProducts']);
    }
  })
  return <>
    <div className="flex flex-row justify-start">
      <label htmlFor={row.original.id} className="">
        <div className="btn bg-green-500 hover:bg-green-700 text-white font-bold mx-1  -my-3 focus:outline-black focus:ring active-bg-green-900 rounded-none">
          <AiOutlineEdit />
        </div>
      </label>
      <input type="checkbox" id={row.original.id} className="modal-toggle" />
      <div className="modal">
        <div className="modal-box">
          <EditFrom data={row.original} />
          <div className="modal-action">
            <label htmlFor={row.original.id} className="btn">Close</label>
          </div>
        </div>
      </div>
      <EditImageModal row={row} />
      <div>
        <button className={`${isDeleting ? "loading loading-spinner" : ""} -my-3 btn bg-red-600 hover:bg-red-700 text-white font-bold focus:outline-black focus:ring active-bg-red-900 rounded-none`}
          onClick={() => deleteProductFn({
            id: row.original.id,
            imagePath: row.original.imageUrl
          })}>
          <AiOutlineDelete />
        </button>
      </div>
    </div>
  </>
}


function EditFrom({ data }: { data: typeMenuProductEdit }) {
  const queryClient = useQueryClient()
  const { mutate: updateProductfn, isLoading: isUpdating } = useMutation(['updateProduct'], updateProduct, {
    onSuccess: () => {
      toast.success('Product Updated')
      queryClient.invalidateQueries(['getProducts']);
    },
    onError: () => toast.error('INTERNAL SERVER ERROR'),
  })
  const { data: categories } = useQuery(['getCategories'], getCategories)
  const { register, handleSubmit } = useForm<typeMenuProductEdit>({ defaultValues: data });
  const onSubmit: SubmitHandler<typeMenuProductEdit> = (fdata) => {
    updateProductfn(fdata)
  };

  return (<>
    <section className="">
      <div className="">
        <form onSubmit={handleSubmit(onSubmit)} className="grid grid-cols-3">
          <div className="col-span-3">
            <label htmlFor="Product Name" className="block text-sm font-medium text-gray-700">
              Product Name
            </label>
            <input
              type="text"
              id="name"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("name")}
            />
          </div>
          <div className="col-span-3">
            <label
              htmlFor="price"
              className="block text-sm font-medium text-gray-700"
            >
              Price
            </label>
            <input
              type="number"
              id="price"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("price")}
            />
          </div>
          <div className="col-span-3 ">
            <label
              htmlFor="description"
              className="block text-sm font-medium text-gray-700"
            >
              Description
            </label>
            <input
              type="text"
              id="description"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("description")}
            />
          </div>
          <div className="col-span-3 ">
            <label
              htmlFor="isAvailable"
              className="block text-sm font-medium text-gray-700"
            >
              Available
            </label>
            <select className="w-full rounded-md border-gray-200" {...register("isAvailable")}>
              <option value={1}>Yes</option>
              <option value={0}>No</option>
            </select>
          </div>
          <div className="col-span-3 ">
            <label
              htmlFor="category"
              className="block text-sm font-medium text-gray-700"
            >
              Category
            </label>
            <select className="w-full rounded-md border-gray-200" {...register("category")}>
              {categories && categories.map((category) => (<option key={category.id} value={category.categoryName}>{category.categoryName}</option>))}
            </select>
          </div>
          <div className="col-span-3 ">
            <label
              htmlFor="size"
              className="block text-sm font-medium text-gray-700"
            >
              Size
            </label>
            <select className="w-full rounded-md border-gray-200" {...register("size")}>
              <option value={"N/A"}>N/A</option>
              <option value={"Small"}>Small</option>
              <option value={"Medium"}>Medium</option>
              <option value={"Large"}>Large</option>
            </select>
          </div>
          <button className={`${isUpdating ? "loading loading-spinner" : ""}  btn bg-black mt-3 hover:bg-gray-700 text-white font-bold focus:outline-black focus:ring active-bg-gray-400 rounded-none`}>
            update
          </button>
        </form>
      </div>
    </section>
  </>)
}

function EditImageModal({ row }: { row: Row<typeViewProductData> }) {
  const queryClient = useQueryClient()
  const [image, setImage] = useState<File>();
  const { data, } = useQuery(['getProductsImage', row.original.imageUrl], () => getProductPicture(row.original.imageUrl))
  const { mutate: reUploadImageFn, isLoading: isReUploading } = useMutation(['reUploadImage'], reUploadImage, {
    onError: () => toast.error('INTERNAL SERVER ERROR'),
    onSuccess: () => {
      toast.success('Image Updated');
      queryClient.invalidateQueries(['getProductsImage']);
    }
  })
  return (<>
    <div className="flex flex-row justify-start">
      <label htmlFor={row.original.created_at} className="">
        <div className="-my-3 btn bg-blue-500 hover:bg-blue-700 text-white font-bold mx-1 focus:outline-black focus:ring active-bg-red-900 rounded-none">
          <MdOutlinePreview />
        </div>
      </label>
      <input type="checkbox" id={row.original.created_at} className="modal-toggle" />
      <div className="modal">
        <div className="modal-box ">
          <div className="flex flex-row justify-between">
            <input type="file" className="file-input file-input-bordered rounded-none w-full max-w-xs"
              onChange={(e) => {
                if (e.target.files === null) {
                  toast.error('No file selected')
                  return;
                }
                setImage(e.target.files[0])
              }
              } />
            <button className={`${isReUploading ? "loading loading-spinner" : ""} btn btn-neutral bg-black px-6 py-2 text-white border border-white focus:border-white focus:ring-4 rounded-none transform transition duration-700 hover:scale-105 `} onClick={() => { image && reUploadImageFn({ image, imagePath: row.original.imageUrl, id: row.original.id }) }}>
              Edit
            </button>
            <label htmlFor={row.original.created_at} className="btn btn-ghost rounded-none bg-transparent border-none">x</label>
          </div>
          <div className="modal-action">
            <div><img src={`${data}?dummy=${crypto.randomUUID()}`} alt="product picture" /></div>
          </div>
        </div>
      </div>
    </div>
  </>
  )
}
