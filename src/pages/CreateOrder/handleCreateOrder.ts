import { typeOrderInsert } from "../../../types";
import { supabaseClient } from "../../utils/supabaseClient";

export async function createOrder({ order, isAdmin, addressDetailId }: { order: typeOrderInsert, isAdmin: boolean, addressDetailId?: string }) {
  if (isAdmin) {
    const { data, error } = await supabaseClient.from('Orders').insert({
      item: order.item,
      isConfirmed: true,
      paid: false,
      itemId: order.itemId,
      price: order.price,
      size: order.size,
      orderMode: "INSHOP",
      customerName: order.customerName,
      quantity: order.quantity,
      orderType: order.orderType,
      addons: order.addons
    }).select()
    if (error) throw error;
    return data;
  }
  const { data, error } = await supabaseClient.from('Orders').insert({
    item: order.item,
    isConfirmed: false,
    paid: false,
    itemId: order.itemId,
    price: order.price,
    customerAddressDetailsId: addressDetailId,
    customerName: order.customerName,
    quantity: order.quantity,
    orderMode: "ONLINE",
    orderType: "ONLINE",
    size: order.size,
    addons: order.addons
}).select()
if (error) throw error
return data
}

export async function viewOnlineOrders() {
  const { data, error } = await supabaseClient.from('Orders').select('*').eq('orderType', 'ONLINE');
  if (error) throw error;
  return data
}

export async function viewOnlineComfirmableOrders() {
  const { data, error } = await supabaseClient.from('Orders').select('*').match({ orderType: 'ONLINE', isConfirmed: false })
  if (error) throw error;
  return data
}
