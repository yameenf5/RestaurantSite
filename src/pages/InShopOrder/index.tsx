import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { ColumnDef, Row } from "@tanstack/react-table";
import { typeOrderEdit, typeOrderView } from "../../../types";
import { OrdersTable } from "../../components/Tables/OrdersTable";
import { completeOrder, deleteOrder, getInshopOrder as getInShopOrders, updateInShopOrder } from "./handleInshopOrders";
import { SubmitHandler, useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import Navbar from "../../components/Navbar";

export function InShopOrders() {
  const { data, isLoading: isfetching } = useQuery(['getInShopOrders'], getInShopOrders)
  const column: ColumnDef<typeOrderView>[] = [
    {
      header: 'Customer Name',
      accessorKey: 'customerName'
    },
    {
      header: 'Product Name',
      accessorKey: 'item'
    },
    {
      header: 'Price In Rupees',
      accessorKey: 'price'
    },
    {
      header: 'Confirmed',
      accessorFn: (row) => row.isConfirmed ? 'Confirmed' : 'Not Confirmed'
    },
    {
      header: 'Paid',
      accessorFn: (row) => row.paid ? 'Paid successfully' : 'Not Paid'
    },
    {
      header: 'Size',
      accessorKey: 'size'
    },
    {
      header: 'Order Type',
      accessorKey: 'orderType'
    },
    {
      header: 'quantity',
      accessorKey: 'quantity'
    },
    {
      header: 'Add-ons',
      accessorFn: (row) => row.addons ? row.addons : 'No Addons'
    },
    {
      header: 'Cancled',
      accessorFn: (row) => row.isCancled ? 'Cancelled' : 'NO'
    },
    {
      header: 'Action',
      cell: ({ row }) => {
        return (<>
          <ActionButtons dataRow={row} />
        </>
        )
      }
    }
  ]

  if (isfetching) return <div className="w-full text-center"><div className="h-1/4 w-1/4  loading loading-bars"></div></div>
  if (data)
    return (<>
      <Navbar />
      <OrdersTable dataArray={data} column={column} />
    </>)
}

function ActionButtons({ dataRow }: { dataRow: Row<typeOrderView> }) {
  const queryClient = useQueryClient()
  const { mutate: deleteOrderFn, isLoading: isDeleting } = useMutation(['deleteIncompleteOrder'], deleteOrder, {
    onSuccess: () => queryClient.invalidateQueries(['getInShopOrders']),
    onError: () => toast.error("INTERNAL SERVER ERROR")
  })
  const { mutate: completeOrderFn, isLoading: isUpdating } = useMutation(['completingOrder'], completeOrder, {
    onSuccess: () => queryClient.invalidateQueries(['getInShopOrders']),
    onError: () => toast.error("INTERNAL SERVER ERROR")
  })

  return (<>
    <div className="flex flex-row justify-start">
      <label htmlFor={dataRow.original.id} className="">
        <div className="btn bg-black hover:bg-gray-700 text-white font-bold mx-1  -my-2 focus:outline-black focus:ring active-gray-300 rounded-none "
        >
          Edit
        </div>
      </label>
      <input type="checkbox" id={dataRow.original.id} className="modal-toggle" />
      <div className="modal">
        <div className="modal-box">
          <UpadateOrderForm item={dataRow.original} />
          <div className="modal-action">
            <label htmlFor={dataRow.original.id} className="btn">Close</label>
          </div>
        </div>
      </div>
      <button className={`${isUpdating ? "loading loading-spinner" : ""}  btn bg-black hover:bg-gray-700 text-white font-bold mx-1  -my-2 focus:outline-black focus:ring active-gray-300 rounded-none `}
        onClick={() => completeOrderFn({ orderId: dataRow.original.id, IsPaid: dataRow.original.paid })}
      >
        Paid
      </button>
      <button className={`${isDeleting ? "loading loading-spinner" : ""}  btn bg-black hover:bg-gray-700 text-white font-bold mx-1  -my-2 focus:outline-black focus:ring active-gray-300 rounded-none `}
        onClick={() => deleteOrderFn({ id: dataRow.original.id, isPaid: dataRow.original.paid })}
      >
        Delete
      </button>
    </div>
  </>)
}


function UpadateOrderForm({ item }: { item: typeOrderEdit }) {
  const queryClient = useQueryClient()
  const { mutate: updateInShopOrderFn, isLoading: isUpdating } = useMutation(['updateInCompleteOrder'], updateInShopOrder,
    {
      onError: () => toast.error("INTERNAL SERVER ERROR"),
      onSuccess: () => {
        queryClient.invalidateQueries(['getInShopOrders']);
      }
    }
  )
  const { register, handleSubmit } = useForm<typeOrderEdit>({
    defaultValues: {
      customerName: item.customerName,
      price: item.price,
      paid: item.paid,
      size: item.size,
      quantity: item.quantity
    }
  });
  const onSubmit: SubmitHandler<typeOrderEdit> = (fdata) => {
    if (item.id === undefined || item.paid === undefined) return;
    updateInShopOrderFn({ updatedOrder: fdata, id: item.id, isPaid: item.paid })
  };

  return (<>
    <section className="">
      <div className="">
        <form onSubmit={handleSubmit(onSubmit)} className="grid grid-cols-3">
          <div className="col-span-3">
            <label htmlFor="CustomerName" className="block text-sm font-medium text-gray-700">
              CustomerName
            </label>
            <input
              type="text"
              id="CustomerName"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("customerName")}
            />
          </div>
          <div className="col-span-3">
            <label
              htmlFor="price"
              className="block text-sm font-medium text-gray-700"
            >
              Price
            </label>
            <input
              type="number"
              id="price"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("price")}
            />
          </div>
          <div className="col-span-3">
            <label
              htmlFor="quantity"
              className="block text-sm font-medium text-gray-700"
            >
              Quantity
            </label>
            <input
              type="number"
              id="price"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("quantity")}
            />
          </div>

          <div className="col-span-3">
            <label
              htmlFor="addons"
              className="block text-sm font-medium text-gray-700"
            >
              Addons
            </label>
            <input
              type="text"
              id="addons"
              className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
              {...register("addons")}
            />
          </div>

          <div className="col-span-3">
            <label
              htmlFor="OrderType"
              className="block text-sm font-medium text-gray-700"
            >
              OrderType
            </label>
            <select className="w-full rounded-md border-gray-200" {...register("orderType")}>
              <option value={"TAKEAWAY"}>Take away</option>
              <option value={"ATRESTRAUANT"}>At restaurant</option>
            </select>
          </div>
          <div className="col-span-3 ">
            <label
              htmlFor="size"
              className="block text-sm font-medium text-gray-700"
            >
              Size
            </label>
            <select className="w-full rounded-md border-gray-200" {...register("size")}>
              <option value={"N/A"}>N/A</option>
              <option value={"Small"}>Small</option>
              <option value={"Medium"}>Medium</option>
              <option value={"Large"}>Large</option>
            </select>
          </div>
          <button className={`${isUpdating ? "loading loading-spinner" : ""}  btn bg-black mt-3 hover:bg-gray-700 text-white font-bold focus:outline-black focus:ring active-bg-gray-400 rounded-none`}>
            Update
          </button>
        </form>
      </div>
    </section>
  </>)
}



