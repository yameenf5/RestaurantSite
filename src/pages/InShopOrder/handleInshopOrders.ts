import { toast } from "react-hot-toast";
import { supabaseClient } from "../../utils/supabaseClient";
import { typeOrderEdit } from "../../../types";

export async function getInshopOrder() {
  const { data } = await supabaseClient.from('Orders').select('*').eq('orderMode', 'INSHOP');
  return data
}

export async function deleteOrder({ id, isPaid }: { id: string, isPaid: boolean }) {
  if (isPaid) {
    toast.error("Completed orders cant be deleted")
  } else {
    const { error } = await supabaseClient.from('Orders').delete().eq('id', id);
    if (error) throw error;
    toast.success("order has been deleted successfully")
  }
}

export async function updateInShopOrder({ updatedOrder, id, isPaid }: { updatedOrder: typeOrderEdit, id: string, isPaid: boolean }) {
  if (isPaid === true) {
    toast.error("Completed orders cant be Edited")
  }
  else {
    const { error } = await supabaseClient.from('Orders').update(updatedOrder).eq('id', id)
    if (error) throw error
    toast.success("order has been updated successfully")
  }
}

export async function completeOrder({ orderId, IsPaid }: { orderId: string, IsPaid: boolean }) {
  if (IsPaid) {
    toast.error("Already paid");
  } else {
    const { error } = await supabaseClient.from('Orders').update({ 'paid': true }).eq('id', orderId);
    if (error) throw error
  }
}


