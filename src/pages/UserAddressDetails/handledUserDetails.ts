import { typeAddressDetailsEdit, typeAddressDetailsInsert } from "../../../types";
import { supabaseClient } from "../../utils/supabaseClient";

export async function getAddressDetaials({ customerId }: { customerId: string | undefined }) {
  if (customerId === undefined) return [];
  const { data, error } = await supabaseClient.from('CustomerDetails').select('*').match({'customerId': customerId, 'isDeleted': false});
  if (data) {
    return data;
  }
  if (error) throw error;
}

export async function addAddressDetails(addreddDetails: typeAddressDetailsInsert) {
  const { error } = await supabaseClient.from('CustomerDetails').insert(addreddDetails).select()
  if (error) throw error;
}

export async function editAddressDetails({ id, updatedDetails }: { id: string, updatedDetails: typeAddressDetailsEdit }) {
  const { error } = await supabaseClient.from('CustomerDetails').update(updatedDetails).eq('id', id)
  if (error) throw error;
}

export async function deleteAddressDetails({ id }: { id: string }) {
  const { error } = await supabaseClient.from('CustomerDetails').update({ isDeleted: true }).eq('id', id)
  if (error) throw error;
}
