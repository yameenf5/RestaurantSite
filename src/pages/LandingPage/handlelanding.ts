import { supabaseClient } from "../../utils/supabaseClient";

export async function getFeaturedItems() {
  const { data } = await supabaseClient.from('MenuProduct').select('*').match({isfeatured: true, isDeleted: false}).range(0, 2);
  console.log(data);
  if (data) return data;
}

