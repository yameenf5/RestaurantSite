import { useMutation, useQuery } from "@tanstack/react-query"
import { getSession } from "../../utils/authSession"
import { getOrderHistory, getOrderStatus } from "./handleOrderHistory"
import Navbar from "../../components/Navbar"
import { typeOrderWithAddress } from "../../../types"
import easyinvoice from "easyinvoice"
import { toast } from "react-hot-toast"

export function OrderHistory() {
  const { data: session } = useQuery(['orderHistory'], getSession)
  const userId = session?.user.id
  const { data: orderHistory } = useQuery(['getOrderHistory', userId], () => getOrderHistory({ userId: userId }))
  return <>
    <Navbar />
    <div className="w-3/4 mx-auto ">
      {orderHistory && orderHistory?.map(order => <PreviousOrder key={order.id} order={order} />)}
    </div>
  </>
}

function PreviousOrder({ order }: { order: typeOrderWithAddress }) {
  const status = getOrderStatus(order)
  const date = new Date(order.created_at)
  const { mutate: generateInvoiceFn, isLoading: generating } = useMutation(['generateInvoice'], GenerateInvoice, {
    onSuccess: () => { toast.success("Invoice Generated") },
    onError: () => { toast.error("Error Generating Invoice") }
  })
  return (<>
    <div className="my-10 ">
      <div className="card card-side rounded-none shadow-xl bg-black ">
        <div className="card-body">
          <h2 className="card-title text-white uppercase">{order.item}</h2>
          <p className="text-white uppercase">Price : {order.price}</p>
          <p className="text-white uppercase">quantity : {order.quantity}</p>
          <p className="text-white uppercase">status: {status}</p>
          <p className="text-white uppercase">Ordered At: {date.toLocaleString()}</p>
          <div className="card-actions justify-end">
            <button
              className={`${generating ? "loading loading-bars" : ""} ${generating? "bg-white": "bg-black"} btn btn-ghost  rounded-none border-white text-white disabled:bg-gray-600 disabled:text-white`}
              onClick={() => (generateInvoiceFn({ data: order }))}
              disabled={(status !== "Delivered")}
            >Invoice</button>
          </div>
        </div>
      </div>
    </div>
  </>)
}


async function GenerateInvoice({ data }: { data: typeOrderWithAddress }) {
  const date = new Date(data.created_at)
  const a = {
    "images": {
      "logo": "https://public.easyinvoice.cloud/img/logo_en_original.png",
      "background": "https://public.easyinvoice.cloud/img/watermark-draft.jpg"
    },
    "sender": {
      "company": "Just Eat",
      "address": "Raj Bagh",
      "city": "Srinagar",
      "country": "India"
    },
    "client": {
      "company": data.customerName,
      "address": `${data.CustomerDetails?.area}`,
      "zip": `${data.CustomerDetails?.pincode}`,
      "city": `${data.CustomerDetails?.city}`,
    },
    "information": {
      "date": `${date.toLocaleDateString()}`,
    },
    "products": [
      {
        "quantity": `${data.quantity}`,
        "description": data.item,
        "tax-rate": 0,
        "price": (data.price/data.quantity),
      },
    ],
    "bottom-notice": "Enjoy your meal!",
    "settings": {
      "currency": "INR",
      "margin-top": 25,
      "margin-right": 25,
      "margin-left": 25,
      "margin-bottom": 25
    }
  }
  const result = await easyinvoice.createInvoice(a)
  easyinvoice.download(`invoice.pdf`, result.pdf)
}
