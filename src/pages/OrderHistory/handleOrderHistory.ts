import { typeOrderView } from "../../../types";
import { supabaseClient } from "../../utils/supabaseClient";


export function getOrderStatus(order: typeOrderView) {
  if (order.paid) return 'Delivered'
  if (order.isCancled) return 'Cancled'
  if (order.isConfirmed) return 'Confirmed'
  return 'Waiting'
}


export async function getOrderHistory({ userId }: { userId: string | undefined }) {
  if(userId === undefined) return [];
  const { data, error } = await supabaseClient.from('Orders').select('*, CustomerDetails (*) ').eq('customerId', userId)
  if (error) throw error
  if (data) return data
}
