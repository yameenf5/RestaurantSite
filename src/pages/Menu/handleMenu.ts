import { toast } from "react-hot-toast";
import { typeOrderInsert, typeViewProductData } from "../../../types";
import { supabaseClient } from "../../utils/supabaseClient";
import { createOrder } from "../CreateOrder/handleCreateOrder";

export async function getMenuItem({ indexValue, category, itemName }: { indexValue: number, category: string, itemName: string }) {
  if (itemName !== '') {
    const { data, error } = await supabaseClient.from('MenuProduct').select('*').eq('isDeleted', false).ilike('name', `%${itemName}%`)
    if (error) throw error;
    if (data) return data;
  }
  if (category === 'All') {
    const { data, error } = await supabaseClient.from('MenuProduct').select('*').eq('isDeleted', false).range(indexValue, indexValue + 2)
    if (error) throw error;
    if (data) return data;
  }
  else {
    const { data, error } = await supabaseClient.from('MenuProduct').select('*').match({ 'category': category, 'isDeleted': false }).range(indexValue, indexValue + 2)
    if (error) throw error;
    if (data) return data;
  }
}

export async function getItemPicture(imageUrl: string) {
  const { data } = supabaseClient.storage.from('Media').getPublicUrl(imageUrl);
  if (imageUrl === "NOTUPLOADED") return "NOTUPLOADED"
  return data.publicUrl;
}

export async function getCatergories() {
  const { data, error } = await supabaseClient.from('Categories').select('*');
  if (error) throw error;
  if (data) return data;
}

export async function getAvailableItemCount() {
  const { count, error } = await supabaseClient.from('MenuProduct').select('*', { count: "exact" }).eq('isAvailable', true);
  if (error) throw error;
  if (count) return count;
}

export async function orderItem({ item, userName, quantity, customerAddressDetailsId }: { item: typeViewProductData, userName: string, quantity: "quantity" | number, customerAddressDetailsId: string }) {
  if (quantity === "quantity") {
    toast.error('Please select quantity')
    return
  }
  if (customerAddressDetailsId === "") { toast.error('Please select address'); return }
  const price = item.price * quantity
  const order: typeOrderInsert = {
    customerName: userName,
    item: item.name,
    itemId: item.id,
    price: price,
    quantity: quantity,
    size: item.size,
    addons: null,
    paid: false,
    isConfirmed: false,
    orderType: 'ONLINE'
  }
  await createOrder({ order: order, isAdmin: false, addressDetailId: customerAddressDetailsId })
}
