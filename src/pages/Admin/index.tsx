import { AdminHeader } from "../../components/Header"
import { SideBar } from "../../components/SideBar"
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query"
import { getCatergories } from "../../pages/Menu/handleMenu"
import { typeCategoriesView, typeOrderView, typeViewProductData } from "../../../types"
import { RiDeleteBin6Fill, RiRestaurantLine } from "react-icons/ri"
import { useState } from "react"
import { NewOnlineOrderCount, addCategory, completedOrderCount, deleteCategory, getCompletedOrders, getFeaturedItem, revenuGenerated, toggleHero, totalItemsCount, unFeature } from "./handleAdmin"
import { toast } from "react-hot-toast"
import { useSupabaseSubscribe } from "../../Hooks/useSupabaseRealtime"
import { ColumnDef, Row } from "@tanstack/react-table";
import { DashboardCompleteOrderTable } from "../../components/Tables/DashBoardCompletedOrderTable"
import { FeaturedTable } from "../../components/Tables/FeaturedTable"
import { BiRupee } from "react-icons/bi"


export function Admin() {
  return (<>
    <div className="flex flex-row  overflow-hidden ">
      <SideBar userName="admin" />
      <div className="flex flex-col w-full">
        <div className="w-full"><AdminHeader userName="admin" /></div>
        <div className="w-full m-2 pl-2 pr-4"><Dashboard /></div>
      </div>
    </div>
  </>)
}

export function Dashboard() {
  const queryClient = useQueryClient();
  const { data } = useQuery(['getCategories'], getCatergories);
  const { data: compOrderCount } = useQuery(['getCompletedOrderCount'], completedOrderCount);
  const { data: newOrderCount } = useQuery(['getNewOrderCount'], NewOnlineOrderCount);
  const { data: itemInfo } = useQuery(['MenuInfo'], totalItemsCount);
  const { data: revenue } = useQuery(['getRevenuInfo'], revenuGenerated);
  const { mutate: addCatergoryFn, isLoading: isAdding } = useMutation(['addCategory'], addCategory, {
    onSuccess: () => {
      toast.success("Category Added")
      queryClient.invalidateQueries(['getCategories'])
    },
    onError: () => { toast.error("INTERNAL SERVER ERROR") }
  })
  const [category, setCategory] = useState('');

  useSupabaseSubscribe({
    channelName: 'OrdersChannel', tableName: 'Orders',
    onUpdate: () => {
      queryClient.invalidateQueries(['getCompletedOrderCount'])
      queryClient.invalidateQueries(['getNewOrderCount'])
      queryClient.invalidateQueries(['getRevenuInfo'])
    },
    onCreate: () => {
      queryClient.invalidateQueries(['getCompletedOrderCount'])
      queryClient.invalidateQueries(['getNewOrderCount'])
      queryClient.invalidateQueries(['getRevenuInfo'])
    },
  }
  )

  return (
    <>
      <div className="flex gap-4 flex-col lg:flex-row">
        <BoxWraper>
          <div className="text-white text-sx font-mono text-center">Revenue Generated :<BiRupee className="inline pb-1"/>{revenue?.totalRevenue} </div>
          <div className="text-white text-sx font-mono text-center">Revenue Generated Online:<BiRupee className="inline pb-1"/>{revenue?.totalOnlineRevenue} </div>
          <div className="text-white text-sx font-mono text-center">Revenue Generated Inshop:<BiRupee className="inline pb-1"/>{revenue?.totalInshopRevenue} </div>
        </BoxWraper>
        <BoxWraper ><div className="text-white text-2xl font-mono text-center"><RiRestaurantLine size={24} className="mx-auto text-red-700" /> Completed Orders : {compOrderCount}</div></BoxWraper>
        <BoxWraper>
          <div className="text-white text-2xl font-mono text-center">Total Items : {itemInfo?.totalItems} </div>
          <div className="text-white text-2xl font-mono text-center">Available Items: {itemInfo?.totalAvailableItems}</div>
        </BoxWraper>
        <BoxWraper>
          <div className="text-white text-2xl font-mono text-center"><RiRestaurantLine size={24} className="mx-auto text-red-700" />New Online Orders : {newOrderCount}</div>
        </BoxWraper>
      </div>
      <div className="flex gap-4 flex-col lg:flex-row ">
        <div className="bg-gray-50 rounded-sm h-80 my-3 inline-block flex-1 border border-black items-center overflow-y-auto ">
          <FeaturedItemComponent />
        </div>
        <div className="bg-gray-200 rounded-sm h-80 my-3 inline-block lg:w-1/3 w-full border border-black items-center  ">
          <div className="text-center text-xl">Customize Categories</div>
          <div className="m-4 lg:mt-2 flex justify-between">
            <input type="text" placeholder="Type..." className="input input-bordered w-full   " value={category}
              onChange={(e) => setCategory(e.target.value)} />
            <button className={`btn btn-outline bg-black text-white ${isAdding ? "loading" : ""} `}
              onClick={() => {
                if (category === '') return toast.error("Category Name is Empty")
                addCatergoryFn({ categoryName: category });
                setCategory('');
              }}>
              Add
            </button>
          </div>
          <div className="text-center border bg-white rounded-2xl m-3 h-52 overflow-y-auto lg:w-3/4 mx-auto">
            <ol>
              {data && data.map((category) => (<li key={category.id} > <Categories category={category} /> </li>))}
            </ol>
          </div>
        </div>
      </div>
      <div className="flex gap-4 flex-col lg:flex-row">
        <div className="bg-gray-50 rounded-sm h-80  inline-block flex-1 border border-black items-center overflow-auto">
          <CompletedOrders />
        </div>
      </div>
    </>
  )
}


function Categories({ category }: { category: typeCategoriesView }) {
  const queryClient = useQueryClient()
  const { mutate: deleteCategoryFn } = useMutation(['deleteCategory'], deleteCategory, {
    onSuccess: () => {
      toast.success("Category Deleted")
      queryClient.invalidateQueries(['getCategories'])
    },
    onError: () => {
      toast.error("INTERNAL SERVER ERROR")
    }
  })
  return <>
    <div className="flex justify-between mx-4 px-5 my-1 text-xl bg-gray-400 rounded-xl text-white">
      {category.categoryName}
      <button onClick={() => { deleteCategoryFn(category.id) }}><RiDeleteBin6Fill className="text-white" size={24} /></button>
    </div>
  </>
}


function BoxWraper({ children }: { children: React.ReactNode }) {
  return (<>
    <div className="bg-gray-800 rounded-sm p-8 my-3 inline-block flex-1 border border-black items-center ">{children}</div>
  </>)
}


function CompletedOrders() {
  const { data, isLoading: isfetching } = useQuery(['getOnlineOrders'], getCompletedOrders)
  const column: ColumnDef<typeOrderView>[] = [
    {
      header: 'Customer Name',
      accessorKey: 'customerName'
    },
    {
      header: 'Product Name',
      accessorKey: 'item'
    },
    {
      header: 'Price In Rupees',
      accessorKey: 'price'
    },
    {
      header: 'Paid',
      accessorFn: (row) => row.paid ? 'Paid successfully' : 'Not Paid'
    },
    {
      header: 'Size',
      accessorKey: 'size'
    },
    {
      header: 'Order Type',
      accessorKey: 'orderType'
    },
    {
      header: 'Order Mode',
      accessorKey: 'orderMode'
    },
    {
      header: 'quantity',
      accessorKey: 'quantity'
    },
    {
      header: 'Add-ons',
      accessorFn: (row) => row.addons ? row.addons : 'No Addons'
    },
  ]

  if (isfetching) return <div className="w-full text-center"><div className="h-1/4 w-1/4  loading loading-bars"></div></div>
  if (data)
    return (<>
      <DashboardCompleteOrderTable dataArray={data} column={column} />
    </>)
}

function FeaturedItemComponent() {
  const { data: featuredItems, isLoading: isfetching } = useQuery(['getFeaturedItems'], getFeaturedItem)
  const column: ColumnDef<typeViewProductData>[] = [
    {
      header: 'Product Name',
      accessorKey: 'name'
    },
    {
      header: 'Category',
      accessorKey: 'category'
    },
    {
      header: 'Hero Item',
      accessorFn: (row) => row.isLargePictureFeatured ? 'YES' : 'NO'
    },
    {
      header: 'Action',
      cell: ({ row }) => {
        return (<>
          <ActionButton row={row} />
        </>
        )
      }
    }
  ]

  if (isfetching) return <div className="w-full text-center"><div className="h-1/4 w-1/4  loading loading-bars"></div></div>
  if (featuredItems)
    return (<>
      <FeaturedTable dataArray={featuredItems} column={column} />
    </>)
}



function ActionButton({ row }: { row: Row<typeViewProductData> }) {
  const queryClient = useQueryClient()
  const { mutate: unFeatureFn } = useMutation(['unFeatureItem'], unFeature, {
    onSuccess: () => {
      toast.success("Item UnFeatured")
      queryClient.invalidateQueries(['getFeaturedItems'])
    },
    onError: () => {
      toast.error("INTERNAL SERVER ERROR")
    }
  })
  const { mutate: toggleHeroFn} = useMutation(['toggleHero'], toggleHero, {
    onSuccess: () => {
      queryClient.invalidateQueries(['getFeaturedItems'])
    },
    onError: () => {
      toast.error("INTERNAL SERVER ERROR")
    }
  })
  return <>
    <div>
      <button className="btn btn-sm  rounded-none p-0 text-xs mx-2 bg-black px-2 text-white btn-outline -mt-2"
        onClick={() => unFeatureFn({ id: row.original.id })}
      >un feature</button>
      <button className="btn btn-sm  rounded-none p-0 text-xs mx-2 bg-black px-2 text-white btn-outline -mt-2"
        onClick={() => toggleHeroFn({ id: row.original.id , isHero: row.original.isLargePictureFeatured})}
      >Toggle Hero</button>
    </div>
  </>
}


