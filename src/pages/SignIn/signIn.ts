import { toast } from "react-hot-toast";
import { typeUserSignIn } from "../../../types";
import { supabaseClient } from "../../utils/supabaseClient";

export async function handleSignIn(signingInUser: typeUserSignIn) {
  const { data: LogedUser, error } = await supabaseClient.auth.signInWithPassword({
    email: signingInUser.email,
    password: signingInUser.password
  })
  if (error) toast.error(error.message);
  if (LogedUser.user !== null) toast.success("you are successfull loged in");
  return LogedUser.user;
}
