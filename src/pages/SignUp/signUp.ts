import type{ typeUser } from "../../../types/index"
import { toast } from "react-hot-toast";
import { supabaseClient } from "../../utils/supabaseClient";

export async function createUserWithPasswordCheck(user: typeUser) {
  if (user.password == user.PasswordConfirmation) {
    return await createUser(user);
  } else {
    throw new Error("Password and Password Confirmation do not match");
  }
}


async function createUser(user: typeUser) {
  const { data: createdUser, error: authError } = await supabaseClient.auth.signUp({
    email: user.email,
    password: user.password,
    options: {
      data: {
        firstName: user.firstName,
        lastName: user.lastName,
        role: "user",
      }
    }
  })
  if(authError === null){
    toast.success('Confirm your Email via link send to your Email');
  }
  if(authError !== null){
    toast.error(authError.message);
  }
  return createdUser;
}


