import { Link } from "react-router-dom";
import Reactlogo from "../../assets/react.svg"
import type { typeAddProduct } from "../../../types";
import { SubmitHandler, useForm } from "react-hook-form";
import { AiOutlineWarning } from "react-icons/ai";
import { useMutation, useQuery } from "@tanstack/react-query";
import { addProduct } from "./handleAddProduct";
import { toast } from "react-hot-toast";
import Navbar from "../../components/Navbar";
import { getCatergories } from "../Menu/handleMenu";

export function AddProduct() {
  const { data: categories } = useQuery(['getCategories'], getCatergories)
  const { mutate: addProductfn, isLoading } = useMutation(['addProduct'], addProduct, {
    onError: () => {
      toast.error("INTERNAL SERVER ERROR");
    },
    onSuccess: () => {
      toast.success("Product Added Successfully");
      reset();
    }
  })
  const { register, handleSubmit, formState: { errors }, reset } = useForm<typeAddProduct>();
  const onSubmit: SubmitHandler<typeAddProduct> = (uploadedItem) => {
    addProductfn(uploadedItem);
  };

  return (<>
    <Navbar />
    <section className="bg-white h-screen ">
      <div className="lg:grid lg:min-h-screen lg:grid-cols-12">
        <section
          className="relative flex h-32 items-end bg-gray-900 lg:col-span-5 lg:h-full xl:col-span-6"
        >
          <img
            alt="Night"
            src="https://images.unsplash.com/photo-1482049016688-2d3e1b311543?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=710&q=80"
            className="absolute inset-0 h-full w-full object-cover opacity-80"
          />

          <div className="hidden lg:relative lg:block lg:p-12">
            <Link className="block text-white" to={"/admin"}>
              <span className="sr-only">Home</span>
              <img className="w-10 cursor-pointer rounded-full bg-white" src={Reactlogo} alt="logo" />
            </Link>

            <h2 className="mt-6 text-2xl font-bold text-white sm:text-3xl md:text-4xl">
              Welcome to JUST EAT {/*TODO*/}
            </h2>

            <p className="mt-4 leading-relaxed text-white">
              Add Item to Menu
            </p>
          </div>
        </section>

        <main
          className="flex items-center justify-center px-8 py-8 sm:px-12 lg:col-span-7 lg:px-16 lg:py-12 xl:col-span-6"
        >
          <div className="max-w-xl lg:max-w-3xl">
            <div className="relative -mt-16 block lg:hidden">
              <a
                className="inline-flex h-16 w-16 items-center justify-center rounded-full bg-white text-blue-600 sm:h-20 sm:w-20"
                href="/"
              >
                <span className="sr-only">Home</span>
                <img src={Reactlogo} />
              </a>
            </div>
            <form onSubmit={handleSubmit(onSubmit)} className="mt-8 grid grid-cols-6 gap-6">
              <div className="col-span-12">
                <label htmlFor="Product Name" className="block text-sm font-medium text-gray-700">
                  Product Name
                </label>
                {errors.name && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.name?.message}</span>}
                <input
                  type="text"
                  id="name"
                  className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
                  {...register("name", { required: "name is require" })}
                />
              </div>

              <div className="lg:col-span-6 col-span-12 ">
                <label
                  htmlFor="category"
                  className="block text-sm font-medium text-gray-700"
                >
                  Category
                </label>
                <select className="w-full rounded-md border-gray-200" {...register("category")}>
                  {categories && categories.map((category) => (<option key={category.id} value={category.categoryName}>{category.categoryName}</option>))}
                </select>
              </div>

              <div className="lg:col-span-6 col-span-12">
                <label
                  htmlFor="price"
                  className="block text-sm font-medium text-gray-700"
                >
                  Price
                </label>
                {errors.price && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.price?.message}</span>}
                <input
                  type="number"
                  id="price"
                  className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
                  {...register("price", {
                    required: "price is required",
                    min: { value: 0, message: "price must be greater than 0" }
                  })}
                />
              </div>

              <div className="col-span-12 ">
                <label
                  htmlFor="description"
                  className="block text-sm font-medium text-gray-700"
                >
                  Description
                </label>
                {errors.description && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.description?.message}</span>}
                <input
                  type="text"
                  id="description"
                  className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
                  {...register("description", {
                    required: "description is required",
                  })}
                />
              </div>
              <div className="col-span-6 ">
                <label
                  htmlFor="isAvailable"
                  className="block text-sm font-medium text-gray-700"
                >
                  Available
                </label>
                <select className="w-full rounded-md border-gray-200" {...register("isAvailable", { required: "select availablity" })}>
                  <option value={1}>Yes</option>
                  <option value={0}>No</option>
                </select>
              </div>
              <div className="col-span-6 ">
                <label
                  htmlFor="size"
                  className="block text-sm font-medium text-gray-700"
                >
                  Size
                </label>
                <select className="w-full rounded-md border-gray-200" {...register("size")}>
                  <option >N/A</option>
                  <option value={"small"}>small</option>
                  <option value={"medium"}>medium</option>
                  <option value={"large"}>large</option>
                </select>
              </div>
              <div className="col-span-12 ">
                <label
                  htmlFor="image"
                  className="block text-sm font-medium text-gray-700"
                >
                  Image
                </label>
                {errors.image && <span className="text-yellow-300 flex"><AiOutlineWarning className="m-1" /> {errors.image?.message}</span>}
                <input type="file" className="file-input file-input-bordered border-gray-200  w-full"
                  {...register("image", { required: "Must upload image" })} />
              </div>
              <div className="col-span-6 sm:flex sm:items-center sm:gap-4">
                <button className="inline-block shrink-0  border border-white bg-black 
                px-12 py-3 text-sm font-medium text-white transition focus:outline-none focus:ring active:text-blue-500" >
                  <div className={`${isLoading ? "loading loading-spinner" : ""} mx-2  `}></div>
                  Add Product
                </button>
              </div>
            </form>
          </div>
        </main>
      </div>
    </section>
  </>)
}


