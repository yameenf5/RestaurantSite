import { useQuery } from "@tanstack/react-query"
import { getSession } from "../utils/authSession"
import { PropsWithChildren } from "react"

type IsDashBoardProp = {
  fallback: React.ReactElement
}
export function IsAdmin({ fallback, children }: PropsWithChildren<IsDashBoardProp>) {
  const { data: Session } = useQuery(['getSession'], getSession)
  if (Session !== null && Session !== undefined) {
    if (Session.user.user_metadata?.role === "admin")
      return <>
        {children}
      </>
  }
  return <>{fallback}</>
}
