import { useMutation, useQueryClient } from '@tanstack/react-query'
import logo from '../../assets/react.svg'
import { signOut } from '../../utils/authSession'
import { toast } from 'react-hot-toast';
import { Link } from 'react-router-dom';

export function AdminHeader({ userName }: { userName: string }) {
  const queryClient = useQueryClient();
  const { mutate: signOutFn } = useMutation(['signOut'], signOut, {
    onSuccess: () => queryClient.invalidateQueries(['getSession']),
    onError: () => toast.error("INTERNAL SERVER ERROR")
  })
  return (<>
    <header>
      <div className="w-full bg-gray-800 py-2  lg:px-1">
        <div className="flex items-center justify-between">
          <div className="text-center sm:text-left">
            <h1 className="text-xl font-sans pl-2 text-white hidden lg:block">
              Welcome Back, Admin!
            </h1>
          </div>
          <div className="my-2 flex flex-row gap-4 mx-1  justify-between">
            <span className="text-sm font-medium">
              <label htmlFor="my-drawer" className="bg-black px-6 py-2 text-white poppins border border-white focus:border-white focus:ring-4 transform transition duration-700 hover:scale-105drawer-button md:hidden ">Nav</label>
            </span>
            <span className="dropdown md:block hidden">
              <label tabIndex={0} className="bg-black ">
                <button
                  type="button"
                  className="group flex shrink-0 items-center rounded-lg transition mx-2 px-2"
                >
                  <span className="sr-only">Menu</span>
                 <Link to={'/'}><img className="w-10 cursor-pointer rounded-full bg-white" src={logo} alt="logo" /></Link>
                  <p className="ms-2 hidden text-white text-left text-lg sm:block ">
                    <strong className="block font-medium uppercase">{userName}</strong>
                  </p>
                </button>
                <ul tabIndex={0} className="dropdown-content z-[1] menu p-2 shadow bg-white rounded-box  w-32">
                  <li><button
                    onClick={() => signOutFn()}
                  >Log out</button></li>
                </ul>
              </label>
            </span>
          </div>
        </div>
      </div>
    </header >
  </>
  )
}


